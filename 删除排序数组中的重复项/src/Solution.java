import org.junit.Test;

/**
 * @author xiaoer
 * @date 2019/12/20 23:47
 */
public class Solution {
    public int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if (nums[i] != nums[j]) {
                i++;
                nums[i] = nums[j];
            }
        }
        return i + 1;
    }

    @Test
    public void test() {
        int nums[] = new int[]{1, 1, 2};
        System.out.println(removeDuplicates(nums));
    }
}
